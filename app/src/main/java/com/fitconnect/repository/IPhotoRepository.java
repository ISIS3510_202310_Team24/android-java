package com.fitconnect.repository;

import android.net.Uri;

import androidx.lifecycle.LiveData;

public interface IPhotoRepository {
    LiveData<String> uploadPhoto(Uri photoUri);
}

