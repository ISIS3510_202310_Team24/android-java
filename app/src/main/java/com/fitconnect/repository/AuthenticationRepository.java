package com.fitconnect.repository;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.fitconnect.livedata.UserLiveData;
import com.fitconnect.model.user.User;
import com.fitconnect.services.firebase.FirebaseConnections;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;

public class AuthenticationRepository {
    private Application application;
    private MutableLiveData<FirebaseUser> firebaseUserMutableLiveData;
    private UserLiveData userLiveData = null;
    public MutableLiveData<FirebaseUser> getFirebaseUserMutableLiveData() {
        return firebaseUserMutableLiveData;
    }
    public AuthenticationRepository(Application application){
        this.application = application;
        firebaseUserMutableLiveData = new MutableLiveData<>();
        if(FirebaseConnections.firebaseAuth().getCurrentUser()!=null){
            firebaseUserMutableLiveData.postValue(FirebaseConnections.firebaseAuth().getCurrentUser());
        }
    }

    public void register(String email, String password, String firstName, String lastName){
        FirebaseConnections.firebaseAuth().createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    firebaseUserMutableLiveData.setValue(FirebaseConnections.firebaseAuth().getCurrentUser());
                    String id = firebaseUserMutableLiveData.getValue().getUid();
                    DocumentReference documentReference = FirebaseConnections.firebaseFirestore().collection("users").document(id);
                    User user = buildUser(firstName, lastName, email);
                    documentReference.set(user.toMap());
                    userLiveData = new UserLiveData(documentReference);
                }else{
                    Toast.makeText(application, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void login(String email, String password){
        FirebaseConnections.firebaseAuth().signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    firebaseUserMutableLiveData.setValue(FirebaseConnections.firebaseAuth().getCurrentUser());
                    String id = firebaseUserMutableLiveData.getValue().getUid();
                    DocumentReference documentReference = FirebaseConnections.firebaseFirestore().collection("users").document(id);
                    userLiveData = getUserById(id);
                }else{
                    Toast.makeText(application, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public User buildUser(String firstName, String lastName, String email){
        User user = new User();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public UserLiveData getUserById(String id){
        DocumentReference documentReference = FirebaseConnections.firebaseFirestore().collection("users").document(id);
        return new UserLiveData(documentReference);
    }

}
