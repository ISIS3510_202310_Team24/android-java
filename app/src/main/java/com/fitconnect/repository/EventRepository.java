package com.fitconnect.repository;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.fitconnect.controller.EventRecommender;
import com.fitconnect.model.dao.EventDao;
import com.fitconnect.model.user.Event;
import com.fitconnect.services.firebase.FirebaseConnections;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EventRepository {
    AppDatabase db;
    EventDao eventDao;

    Context context;

    String documentId;

    private FirebaseFirestore firestoreDb = FirebaseConnections.firebaseFirestore();
    private MutableLiveData<List<Event>> events = new MutableLiveData<>();
    private MutableLiveData<HashMap<String, Integer>> sportsCount = new MutableLiveData<>();
    private MutableLiveData<Integer> countLiveData = new MutableLiveData<>(0);

    public EventRepository(Context context) {
        this.context = context;
        this.db = Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class, "fitconnect-db").build();
        eventDao = db.eventDao();
    }

    public Task<Void> createEvent(String sport, int playersNeeded, int playersBrought, String eventsDate, String eventsTime, String duration, String location){
        Calendar calendar = Calendar.getInstance();
        Date currentTime = calendar.getTime();
        LocalTime time = LocalTime.parse(eventsTime);
        LocalDate date = LocalDate.parse(eventsDate);
        LocalDateTime startDate = time.atDate(date);
        String[] durationArr = duration.split(":");
        LocalDateTime endDate = startDate.plusMinutes(Long.parseLong(durationArr[1]));
        endDate = endDate.plusHours(Long.parseLong(durationArr[0]));
        Instant instantEnd = endDate.atZone(ZoneId.systemDefault()).toInstant();
        Date endDateD = Date.from(instantEnd);
        Instant instantStart = startDate.atZone(ZoneId.systemDefault()).toInstant();
        Date startDateD = Date.from(instantStart);
        Map<String, Object> event = new HashMap<>();
        List<String> participants = new ArrayList<>();
        event.put("createdAt", currentTime);
        event.put("endDate", endDateD);
        event.put("eventOwner", FirebaseConnections.firebaseAuth().getUid());
        event.put("location", location);
        event.put("participants", participants);
        event.put("playersBrought", playersBrought);
        event.put("playersNeeded", playersNeeded);
        event.put("sport", sport);
        event.put("spotsAvailable", playersNeeded);
        event.put("startDate", startDateD);
        DocumentReference newEvent = firestoreDb.collection("events").document();
        return newEvent.set(event);
    }

    private Date getDateNDaysAgo(int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -n);
        return calendar.getTime();
    }

    public void insertEventsInRoom(List<Event> events) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            Event[] eventsArray = events.toArray(new Event[0]);
            eventDao.deleteAll();//To avoid SQL constraints
            eventDao.insertAll(eventsArray);
        });
        executorService.shutdown();
    }

    public MutableLiveData<HashMap<String, Integer>> getSportsCount(){
        return sportsCount;
    }

    private void getEventsFromRoom() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            events.postValue(eventDao.getAll());
        });
        executorService.shutdown();
    }

    public void deleteAllEvents() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            eventDao.deleteAll();
        });
        executorService.shutdown();
    }

    private List<Event> getEventsByOwnerIdInRoom(String userId) {
        return eventDao.getEventsByOwnerId(userId);
    }

    public void getEventsBySportAndWeek(){
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
            String userId = FirebaseConnections.firebaseAuth().getCurrentUser().getUid();
            HashMap<String, Integer> sportList = new HashMap<>();
            if (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                CollectionReference collection = firestoreDb.collection("events");
                collection.whereEqualTo("eventOwner", userId)
                        .whereGreaterThanOrEqualTo("createdAt", getDateNDaysAgo(7))
                        .orderBy("createdAt")
                        .orderBy("sport")
                        .get()
                        .addOnCompleteListener(task ->{
                            if (task.isSuccessful()){
                                ExecutorService executorService = Executors.newSingleThreadExecutor();
                                executorService.execute(() -> {
                                    for (QueryDocumentSnapshot document : task.getResult()){
                                        Event event = document.toObject(Event.class);
                                        if (sportList.containsKey(event.getSport())){
                                            sportList.put(event.getSport(), sportList.get(event.getSport())+1);
                                        }else{
                                            sportList.put(event.getSport(), 1);
                                        }
                                    }
                                    sportsCount.postValue(sportList);
                                });
                                executorService.shutdown();
                            }
                        });
            }
            //else {}
    }
    private Task<List<Event>> getPastEventsByUserOwnerId() {
        TaskCompletionSource<List<Event>> taskCompletionSource = new TaskCompletionSource<>();
        Tasks.call(Executors.newSingleThreadExecutor(), () -> {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
            String userId = FirebaseConnections.firebaseAuth().getCurrentUser().getUid();
            if (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                CollectionReference collection = firestoreDb.collection("events");
                collection.whereEqualTo("eventOwner", userId)
                        .get()
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                List<Event> eventsList = new ArrayList<Event>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Calendar c = Calendar.getInstance();
                                    Event event = document.toObject(Event.class);
                                    event.setId(document.getId());
                                    if (event.getEndDate().compareTo(c.getTime()) >0) {
                                        eventsList.add(event);
                                    }
                                }
                                taskCompletionSource.setResult(eventsList);

                            } else {
                                taskCompletionSource.setException(task.getException());
                            }
                        });
            } else {
                List<Event> eventsList = getEventsByOwnerIdInRoom(userId);
                taskCompletionSource.setResult(eventsList);
            }
            return null;
        }).addOnFailureListener(taskCompletionSource::setException);

        return taskCompletionSource.getTask();
    }


    public void getEventFailures(){
        Executor executor = Executors.newSingleThreadExecutor();
        Task<List<Event>> task = getPastEventsByUserOwnerId();
        task.addOnSuccessListener(executor, events -> {
            int count = 0;
            for (Event event: events){
                if (event.getParticipants().size() < event.getPlayersNeeded()){
                    count++;
                }
            }
            countLiveData.postValue(count);
        });
        task.addOnFailureListener(executor, exception -> {
            // Maneja las excepciones aquí
        });
    }

    public MutableLiveData<Integer> getCountLiveData() {
        return countLiveData;
    }

    public LiveData<List<Event>> getEvents() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
        if (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
            firestoreDb.collection("events")
                    .whereGreaterThan("startDate", getDateNDaysAgo(30))
                    .addSnapshotListener((queryDocumentSnapshots, e) -> {
                        if (e != null) {
                            events.setValue(null);
                            return;
                        }
                        List<Event> eventsList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Event event = document.toObject(Event.class);
                            event.setId(document.getId());
                            eventsList.add(event);
                        }
                        events.setValue(eventsList);

                        insertEventsInRoom(eventsList);
                    });
        } else {
            getEventsFromRoom();

            Toast.makeText(context, "Showing offline events", Toast.LENGTH_SHORT).show();
        }
        return events;
    }

    public LiveData<List<Event>> getEventsByUserOld(String userEmail) {
        MutableLiveData<List<Event>> events = new MutableLiveData<>();

        firestoreDb.collection("users")
                .whereEqualTo("email", userEmail)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        DocumentSnapshot userDoc = task.getResult().getDocuments().get(0);

                        String userId = userDoc.getId();
                        Log.d("Log-userId", "getEventsByUser: " + userId);

                        ExecutorService executorService = Executors.newSingleThreadExecutor();
                        executorService.execute(() -> {
                            List<Event> eventsList = eventDao.getEventsByParticipant(userId);
                            Log.d("Log-InsideExecutor", "executor");
                            events.postValue(eventsList);
                            Log.d("Log-InsideExecutor", "value was added");
                        });
                        executorService.shutdown();
                    } else {
                        events.setValue(null);
                    }
                });

        return events;
    }




    public LiveData<List<Event>> getEventsByUser(String userEmail) {
        MutableLiveData<List<Event>> events = new MutableLiveData<>();

        firestoreDb.collection("users")
                .whereEqualTo("email", userEmail)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        DocumentSnapshot userDoc = task.getResult().getDocuments().get(0);

                        String userId = userDoc.getId();
                        Log.d("Log-userId", "getEventsByUser: " + userId);

                        firestoreDb.collection("events")
                                .whereArrayContains("participants", userId)
                                .get()
                                .addOnCompleteListener(task1 -> {
                                    if (task1.isSuccessful()) {
                                        List<Event> eventsList = new ArrayList<>();
                                        for (QueryDocumentSnapshot document : task1.getResult()) {
                                            Event event = document.toObject(Event.class);
                                            eventsList.add(event);
                                        }
                                        events.postValue(eventsList);
                                    } else {
                                        events.setValue(null);
                                    }
                                });

                    } else {
                        events.setValue(null);
                    }
                });

        return events;
    }


    public LiveData<Event> getRecommendedEvent(String userEmail) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
        if (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {

        MutableLiveData<Event> recommendedEvent = new MutableLiveData<>();

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        firestoreDb.collection("users")
                .whereEqualTo("email", userEmail)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        DocumentSnapshot userDoc = task.getResult().getDocuments().get(0);

                        String userId = userDoc.getId();

                        executorService.execute(() -> {
                            List<Event> allEvents = eventDao.getAll();
                            List<Event> userEvents = new ArrayList<>();
                            for (Event e : allEvents) {
                                if(e.getParticipants().contains(userId))
                                    userEvents.add(e);
                            }
                            EventRecommender eventRecommender = new EventRecommender(allEvents, userEvents);
                            recommendedEvent.postValue(eventRecommender.recommendEvent());
                        });
                    }
                });
        return recommendedEvent;
        }else{
            //return null;  return null is problematic
            MutableLiveData<Event> noConnection = new MutableLiveData<>();
            Event event = new Event();
            event.setSport("No connection available");
            noConnection.postValue(event);
            return noConnection;
        }
    }


    public LiveData<String> addParticipantToEvent(String userEmail, String eventId) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());

        MutableLiveData<String> available = new MutableLiveData<>();

        if (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();

            // Buscar el usuario por correo electrónico
            db.collection("users")
                    .whereEqualTo("email", userEmail)
                    .get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            DocumentSnapshot userSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                            String userId = userSnapshot.getId();

                            // Verificar si el usuario ya se encuentra en la lista de participantes
                            db.collection("events").document(eventId)
                                    .get()
                                    .addOnSuccessListener(eventSnapshot -> {
                                        Event event = eventSnapshot.toObject(Event.class);
                                        List<String> participants = event.getParticipants();
                                        Date endDate = event.getEndDate();

                                        if (endDate != null && endDate.before(new Date())) {
                                            // El evento ya ha finalizado
                                            available.setValue("Event is finished");
                                        } else if (participants != null && participants.contains(userId)) {
                                            // El usuario ya se encuentra en la lista de participantes
                                            available.setValue("Already added");
                                        } else {
                                            // Actualizar el evento con el ID del usuario en el arreglo de participantes
                                            DocumentReference eventRef = db.collection("events").document(eventId);
                                            eventRef.update("participants", FieldValue.arrayUnion(userId))
                                                    .addOnSuccessListener(aVoid -> {
                                                        // Participante agregado exitosamente al evento
                                                        //Toast.makeText(context, "Joined!!", Toast.LENGTH_SHORT).show();
                                                        Log.d("EventRepository", "Participant added to event");
                                                        available.setValue("added");
                                                    })
                                                    .addOnFailureListener(e -> {
                                                        // Error al agregar el participante al evento
                                                        Log.e("EventRepository", "Failed to add participant to event", e);
                                                    });
                                        }
                                    })
                                    .addOnFailureListener(e -> {
                                        // Error al obtener el evento de Firestore
                                        Log.e("EventRepository", "Failed to fetch event", e);
                                    });
                        } else {
                            // No se encontró ningún usuario con el correo electrónico proporcionado
                            Log.d("EventRepository", "User not found");
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Error al buscar el usuario en Firestore
                        Log.e("EventRepository", "Failed to fetch user", e);
                    });
        } else {
            available.setValue("No internet available");
        }

        return available;
    }


}

