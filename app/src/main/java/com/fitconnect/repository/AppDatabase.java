package com.fitconnect.repository;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;
import com.fitconnect.model.dao.EventDao;
import com.fitconnect.model.user.Event;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

@Database(entities = {Event.class}, version = 2)
@TypeConverters({AppDatabase.StringListTypeConverter.class, AppDatabase.DateTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract EventDao eventDao();

    public static class StringListTypeConverter {
        @TypeConverter
        public static List<String> fromString(String value) {
            Type listType = new TypeToken<List<String>>() {
            }.getType();
            return new Gson().fromJson(value, listType);
        }

        @TypeConverter
        public static String fromList(List<String> list) {
            Gson gson = new Gson();
            return gson.toJson(list);
        }
    }

    public static class DateTypeConverter {
        @TypeConverter
        public static Date toDate(Long timestamp) {
            return timestamp == null ? null : new Date(timestamp);
        }

        @TypeConverter
        public static Long toTimestamp(Date date) {
            return date == null ? null : date.getTime();
        }
    }
}
