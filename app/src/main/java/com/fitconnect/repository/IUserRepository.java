package com.fitconnect.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.fitconnect.model.user.Achievement;
import com.fitconnect.model.user.User;

public interface IUserRepository {
    LiveData<User> getUserByEmail(String correo);

    MutableLiveData<Boolean> updateUserProfile(String email, String photoUrl);

    LiveData<Achievement> getAchievementById(String achievementId);
}
