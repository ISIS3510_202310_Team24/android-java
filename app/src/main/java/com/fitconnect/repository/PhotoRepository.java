package com.fitconnect.repository;

import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.UUID;

public class PhotoRepository implements IPhotoRepository {
    private static final String PHOTOS_FOLDER = "photos/";
    private StorageReference storageRef;

    public PhotoRepository() {
        storageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public LiveData<String> uploadPhoto(Uri photoUri) {
        MutableLiveData<String> photoUrlLiveData = new MutableLiveData<>();

        StorageReference photoRef = storageRef.child(PHOTOS_FOLDER + UUID.randomUUID().toString());
        photoRef.putFile(photoUri)
                .addOnSuccessListener(taskSnapshot -> photoRef.getDownloadUrl()
                        .addOnSuccessListener(uri -> photoUrlLiveData.setValue(uri.toString())))
                .addOnFailureListener(e -> photoUrlLiveData.setValue(null));

        return photoUrlLiveData;
    }
}

