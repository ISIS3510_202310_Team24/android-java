package com.fitconnect.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.fitconnect.model.user.Achievement;
import com.fitconnect.model.user.User;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.HashMap;
import java.util.Map;

public class UserRepository implements IUserRepository {
    private FirebaseFirestore db;
    private MutableLiveData<User> usuarioLiveData;

    public UserRepository() {
        db = FirebaseFirestore.getInstance();
        usuarioLiveData = new MutableLiveData<>();
    }

    @Override
    public LiveData<User> getUserByEmail(String correo) {
        db.collection("users")
                .whereEqualTo("email", correo)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            User usuario = document.toObject(User.class);
                            usuarioLiveData.setValue(usuario);
                        }
                    } else {
                        Log.d("ERROR: " +
                                "FirebaseUserRepository", "Error getting documents: ", task.getException());
                    }
                });
        return usuarioLiveData;
    }

    public MutableLiveData<Boolean> updateUserProfile(String email, String photoUrl) {
        MutableLiveData<Boolean> isSuccessful = new MutableLiveData<>();

        db.collection("users")
                .whereEqualTo("email", email)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot userSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String userId = userSnapshot.getId();

                        Map<String, Object> updates = new HashMap<>();
                        updates.put("profilePicture", photoUrl);

                        db.collection("users").document(userId)
                                .update(updates)
                                .addOnSuccessListener(aVoid -> isSuccessful.setValue(true))
                                .addOnFailureListener(e -> isSuccessful.setValue(false));
                    }
                })
                .addOnFailureListener(e -> isSuccessful.setValue(false));

        return isSuccessful;
    }

    public LiveData<Achievement> getAchievementById(String achievementId) {
        MutableLiveData<Achievement> achievementLiveData = new MutableLiveData<>();

        db.collection("achievements")
                .document(achievementId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Achievement achievement = document.toObject(Achievement.class);
                            achievementLiveData.setValue(achievement);
                        } else {
                            achievementLiveData.setValue(null);
                        }
                    } else {
                        achievementLiveData.setValue(null);
                    }
                });

        return achievementLiveData;
    }


}

