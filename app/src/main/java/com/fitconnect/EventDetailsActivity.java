package com.fitconnect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fitconnect.model.user.Event;
import com.fitconnect.viewmodel.ExploreViewModel;
import com.fitconnect.viewmodel.ViewModelFactory;

public class EventDetailsActivity extends AppCompatActivity {

    private ExploreViewModel exploreViewModel;

    TextView sport, place, startdate, enddate, playersneeded, spotsavailable;
    Button joinButton;

    String userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        Intent intent = getIntent();
        Event event = (Event) intent.getSerializableExtra("event");
        configView();
        sport.setText(event.getSport());
        place.setText(event.getLocation());
        startdate.setText(event.getStartDate().toString());
        enddate.setText(event.getEndDate().toString());
        playersneeded.setText(String.valueOf(event.getPlayersNeeded()));
        spotsavailable.setText(String.valueOf(event.getSpotsAvailable()));

        exploreViewModel = new ViewModelProvider(this, new ViewModelFactory(getApplication())).get(ExploreViewModel.class);

        SharedPreferences sharedPref = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
        userEmail = sharedPref.getString("email", "No hay email");

        joinButton.setOnClickListener(v -> {
            exploreViewModel.addParticipantToEvent(userEmail, event.getId()).observe(this, s -> {
                switch (s) {
                    case "No internet available":
                        Toast.makeText(this, "Feature not available without connection", Toast.LENGTH_SHORT).show();
                        break;
                    case "Event is finished":
                        Toast.makeText(this, "You can't join because the event already ended", Toast.LENGTH_SHORT).show();
                        break;
                    case "Already added":
                        Toast.makeText(this, "Already added!!", Toast.LENGTH_SHORT).show();
                        break;
                    case "added":
                        Toast.makeText(this, "Joined!!", Toast.LENGTH_SHORT).show();
                        break;
                }
            });
        });
    }

    private void configView() {
        sport = findViewById(R.id.sportdata);
        place = findViewById(R.id.placedata);
        startdate = findViewById(R.id.startdatedata);
        enddate = findViewById(R.id.enddatedata);
        playersneeded = findViewById(R.id.playersneededdata);
        spotsavailable = findViewById(R.id.spotsavailabledata);
        joinButton = findViewById(R.id.joinButton);
    }
}