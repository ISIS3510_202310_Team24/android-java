package com.fitconnect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import android.app.DatePickerDialog;
import android.widget.Spinner;
import android.widget.Toast;

import com.fitconnect.viewmodel.EventViewModel;
import com.fitconnect.views.NumberPickerDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Calendar;
import java.util.Locale;

public class CreateEventActivity extends AppCompatActivity implements NumberPickerDialog.OnNumberSetListener{
    private EditText eventDateInput;
    private EditText durationInput;
    private EditText eventDateTimeInput;
    private EditText locationInput;
    private EventViewModel eventViewModel;
    private String sport;
    private String playersBrought;
    private String playersNeeded;
    private String date;
    private String hour;
    private String duration;
    private String location;
    private Spinner sportSpinner;
    private Spinner playersBroughtSpinner;
    private Spinner playersNeededSpinner;
    private FloatingActionButton fab;
    private boolean isConnected = true;
    private final BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            createEventsOffline(isNetworkConnected());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        Toolbar toolbar = findViewById(R.id.createSportsToolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
        sportSpinner = findViewById(R.id.sportSpinner);
        playersBroughtSpinner = findViewById(R.id.broughtPlayersSpinner);
        playersNeededSpinner = findViewById(R.id.neededPlayersSpinner);
        locationInput = findViewById(R.id.locationEditText);
        eventDateInput = findViewById(R.id.dateEditText);
        eventDateInput.setOnClickListener(view ->{
            if (view.getId() == R.id.dateEditText) {
                showDatePickerDialog();
            }
        });
        eventDateTimeInput = findViewById(R.id.dateTimeEditText);
        eventDateTimeInput.setOnClickListener(view ->{
            if (view.getId() == R.id.dateTimeEditText){
                showTimePickerDialog();
            }
        });
        durationInput = findViewById(R.id.durationEditText);
        durationInput.setOnClickListener(view -> showNumberPickerDialog());
        eventViewModel = new ViewModelProvider(this).get(EventViewModel.class);
        fab = findViewById(R.id.fabCreate);
        fab.setOnClickListener(view -> {
            sport = (String) sportSpinner.getSelectedItem();
            playersBrought = (String)playersBroughtSpinner.getSelectedItem();
            playersNeeded = (String)playersNeededSpinner.getSelectedItem();
            date = eventDateInput.getText().toString();
            hour = eventDateTimeInput.getText().toString();
            duration = durationInput.getText().toString();
            location = locationInput.getText().toString();
            if (checkAllFields()){
                if (isConnected){
                    fab.setEnabled(false);
                    fab.setImageResource(R.drawable.baseline_block_24);
                    Snackbar.make(this.findViewById(android.R.id.content), "Event is being created...", Snackbar.LENGTH_SHORT).show();
                    eventViewModel.createEvent(sport, Integer.parseInt(playersNeeded), Integer.parseInt(playersBrought), date, hour, duration, location)
                            .addOnSuccessListener(unused -> {
                                showCreationSuccessDialog();
                            })
                            .addOnFailureListener(e -> {
                                showCreationFailureDialog();
                            });
                }else{
                    showSaveOfflineDialog();
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_event_menu, menu);
        return true;
    }

    public void itemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.create_event_restore){
            SparseArray<Object> event = eventViewModel.restoreEventSavedOffline();
            if (!event.get(0, "").equals("")){
                sportSpinner.setSelection((int)event.get(0));
                playersNeededSpinner.setSelection((int)event.get(1));
                playersBroughtSpinner.setSelection((int)event.get(2));
                eventDateInput.setText((String)event.get(3));
                eventDateTimeInput.setText((String) event.get(4));
                durationInput.setText((String) event.get(5));
                locationInput.setText((String) event.get(6));
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
                builder.setMessage("There is no content to be restored")
                        .setPositiveButton("OK", (dialogInterface, i) -> {
                            dialogInterface.dismiss();
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, filter);
    }

    @Override
    public void onPause(){
        super.onPause();
        unregisterReceiver(networkReceiver);
    }

    private void showSaveOfflineDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
        builder.setMessage("You are offline but you can save your changes for later! If you have previously saved changes, those will be overwritten")
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    eventViewModel.saveEventForLaterOffline(sportSpinner.getSelectedItemPosition(), playersNeededSpinner.getSelectedItemPosition(), playersBroughtSpinner.getSelectedItemPosition(), date, hour, duration, location);
                    dialogInterface.dismiss();

                })
                .setNegativeButton("Cancel", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showCreationFailureDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
        builder.setMessage("Your event could not be created, please try again")
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    clearFields();
                    fab.setEnabled(true);
                    fab.setImageResource(R.drawable.baseline_add_24);
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    private void showCreationSuccessDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
        builder.setMessage("Event has been created successfully!")
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    this.finish();
                })
                .setNegativeButton("Create another one", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    clearFields();
                    fab.setEnabled(true);
                    fab.setImageResource(R.drawable.baseline_add_24);
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private boolean isNetworkConnected(){
        ConnectivityManager cM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cM.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnected();
    }

    private void createEventsOffline(boolean enable){
        if (!enable && isConnected){
            fab.setImageResource(R.drawable.baseline_front_hand_24);
            Snackbar.make(findViewById(android.R.id.content), "You have no connection, wait until it comes back to create event", Snackbar.LENGTH_SHORT).show();
            isConnected = false;
        }else if(enable && !isConnected){
            fab.setImageResource(R.drawable.baseline_add_24);
            Snackbar snackConnection = Snackbar.make(findViewById(android.R.id.content), "Reconnected to your network", Snackbar.LENGTH_LONG);
            snackConnection.setBackgroundTint(getColor(R.color.celestial_blue));
            snackConnection.show();
            isConnected = true;
        }
    }

    private boolean checkAllFields(){
        if(sport.equals("Select a sport")){
            Toast.makeText(CreateEventActivity.this, "Select a valid sport", Toast.LENGTH_SHORT).show();
        }else if (playersBrought.equals("Select the number of players")){
            Toast.makeText(CreateEventActivity.this, "Select a valid number of players brought", Toast.LENGTH_SHORT).show();
        }else if (playersNeeded.equals("Select a number of players")){
            Toast.makeText(CreateEventActivity.this, "Select a valid number of players needed", Toast.LENGTH_SHORT).show();
        }else if(date.length()==0){
            eventDateInput.setError("Select a date to create an event");
        }else if(hour.length()==0){
            eventDateTimeInput.setError("Select an hour to create an event");
        }else if(duration.length()==0){
            durationInput.setError("Select the duration of the event");
        }else if (location.length()==0){
            locationInput.setError("Type the event's location");
        }else{
            return true;
        }
        return false;
    }

    private void showNumberPickerDialog() {
        NumberPickerDialog numberPickerDialog = new NumberPickerDialog();
        numberPickerDialog.setOnNumberSetListener(this);
        numberPickerDialog.show(getSupportFragmentManager(), "NumberPickerDialog");
    }

    @Override
    public void onNumberSet(int hours, int minutes) {
        String time = String.format(Locale.getDefault(), "%02d:%02d", hours, minutes);
        durationInput.setText(time);
    }

    private void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog  newFragment = new DatePickerDialog (CreateEventActivity.this, (view, year1, monthOfYear, dayOfMonth) -> {
            // on below line we are setting date to our text view.
            if(monthOfYear < 9){
                eventDateInput.setText(year1 + "-" + "0" + (monthOfYear + 1) + "-" + dayOfMonth);
            }else{
                eventDateInput.setText(year1 + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
            }


        }, year, month, day);
        newFragment.getDatePicker().setMinDate(c.getTimeInMillis());
        newFragment.show();
    }

    private void showTimePickerDialog(){
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(CreateEventActivity.this,
                (view, hourOfDay, minute1) -> {
                    // on below line we are setting selected time
                    // in our text view.
                    eventDateTimeInput.setText(hourOfDay + ":" + minute1);
                }, hour, minute, false);
        // at last we are calling show to
        // display our time picker dialog.
        timePickerDialog.show();
    }

    private void clearFields(){
        eventDateInput.getText().clear();
        eventDateTimeInput.getText().clear();
        durationInput.getText().clear();
        locationInput.getText().clear();
        sportSpinner.setSelection(0);
        playersBroughtSpinner.setSelection(0);
        playersNeededSpinner.setSelection(0);
    }
}