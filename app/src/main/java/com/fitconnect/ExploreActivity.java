package com.fitconnect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.fitconnect.controller.BrightnessController;
import com.fitconnect.controller.BrightnessObserver;
import com.fitconnect.controller.EventRecommender;
import com.fitconnect.controller.EventsAdapter;
import com.fitconnect.controller.NavigationHandler;
import com.fitconnect.model.user.Event;
import com.fitconnect.viewmodel.ExploreViewModel;
import com.fitconnect.viewmodel.SettingsViewModel;
import com.fitconnect.viewmodel.ViewModelFactory;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class ExploreActivity extends AppCompatActivity implements BrightnessObserver {

    private RecyclerView recyclerView;
    private EventsAdapter eventsAdapter;
    private ExploreViewModel exploreViewModel;

    private BottomNavigationView bottomNavigationView;

    ImageView imageView;

    ImageView pieChart;

    List<Event> eventList;

    String emailFronSP;
    private SettingsViewModel settingsViewModel;
    private BrightnessController brightnessController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);

        configView();

        //Leyendo desde sharedPreferences
        SharedPreferences sharedPref = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
        emailFronSP = sharedPref.getString("email", "No hay email");
        //Log.d("sharedPref", "onCreate: "+emailFronSP);

        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.navigation_explore);
        NavigationHandler.setupWith(bottomNavigationView, this);

        recyclerView = findViewById(R.id.events_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        exploreViewModel = new ViewModelProvider(this, new ViewModelFactory(getApplication())).get(ExploreViewModel.class);
        exploreViewModel.getEvents().observe(this, events -> {
            if (events != null) {
                eventsAdapter = new EventsAdapter(events);
                recyclerView.setAdapter(eventsAdapter);
                eventList=events;
                Log.d("Log-Events", "onCreate: "+eventList.size());

                eventsAdapter.setOnItemClickListener(new EventsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Event event) {
                        // Crear un intent y enviar los datos del evento a la actividad de destino
                        Intent intent = new Intent(ExploreActivity.this, EventDetailsActivity.class);
                        intent.putExtra("event", event);
                        startActivity(intent);
                    }
                });
            } else {
                Toast.makeText(this, "Failed to retrieve events.", Toast.LENGTH_SHORT).show();
            }
        });

        imageView.setOnClickListener(v -> {
            exploreViewModel.getRecommendedEvent(emailFronSP).observe(this, recomendado -> {
                if(recomendado.getSport().equals("No connection available")) {
                    Toast.makeText(this, "Action not available offline", Toast.LENGTH_SHORT).show();
                }else {
                    Intent i = new Intent(this, RecommendedEventActivity.class);
                    i.putExtra("titleTextView", recomendado.getSport());
                    i.putExtra("location", recomendado.getLocation());
                    i.putExtra("dateTextView", recomendado.getStartDate().toString());
                    i.putExtra("availableSpots", recomendado.getSpotsAvailable());
                    startActivity(i);
                }
            });
        });

        pieChart.setOnClickListener(v -> {
            exploreViewModel.getEventsByUser(emailFronSP).observe(this, events -> {
                if(events!=null){
                    Intent i = new Intent(this, EventsDistributionActivity.class);
                    i.putExtra("eventList", events.toArray(new Event[0]));
                    startActivity(i);
                }
            });
        });



        settingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        brightnessController = new BrightnessController(this);

    }


    void configView(){
        imageView = findViewById(R.id.imageView6);
        pieChart = findViewById(R.id.imageView7);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNavigationView.setSelectedItemId(R.id.navigation_explore);
        if (settingsViewModel.getBrightnessControlModePref()){
            brightnessController.start();
            brightnessController.addObserver(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (settingsViewModel.getBrightnessControlModePref()){
            brightnessController.stop();
            brightnessController.removeObserver(this);
        }
    }

    @Override
    public void onBrightnessChanged(float brightness) {
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();

        if (brightness < 15) {
            // Baja la luminosidad de la pantalla en un 20%
            layoutParams.screenBrightness = 0.2f;
        } else if (brightness < 530) {
            // Mantiene la luminosidad de la pantalla igual
            layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
        } else {
            // Aumenta la luminosidad de la pantalla en un 20%
            layoutParams.screenBrightness = 1f;
        }
        getWindow().setAttributes(layoutParams);
    }
}