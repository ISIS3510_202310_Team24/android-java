package com.fitconnect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class RecommendedEventActivity extends AppCompatActivity {

    private TextView titleTextView;
    private TextView dateTextView;

    private TextView availableSpots;
    private TextView location;

    private ImageView imageView;
    private CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommended_event);

        configView();

        Intent intent = getIntent();
        String eventTitle = intent.getStringExtra("titleTextView");
        String eventLocation = intent.getStringExtra("location");
        String eventDateTextView = intent.getStringExtra("dateTextView");
        String eventAvailableSpots = intent.getStringExtra("availableSpots");

        titleTextView.setText(eventTitle);
        location.setText(eventLocation);
        dateTextView.setText(eventDateTextView);
        availableSpots.setText(eventAvailableSpots);

        int sportImageResource = 0;
        if (eventTitle != null) {
            switch (eventTitle.toLowerCase()) {
                case "hockey":
                    sportImageResource = R.drawable.hockey_background;
                    break;
                case "volleyball":
                    sportImageResource = R.drawable.volleyball_background;
                    break;
                case "boxing":
                    sportImageResource = R.drawable.boxing_background;
                    break;
                case "baseball":
                    sportImageResource = R.drawable.baseball_background;
                    break;
                case "basketball":
                    sportImageResource = R.drawable.basketball_background;
                    break;
                case "tennis":
                    sportImageResource = R.drawable.tennis_background;
                    break;
                case "football":
                    sportImageResource = R.drawable.football_background;
                    break;
                // Agrega más casos según los deportes que quieras
                default:
                    sportImageResource = R.drawable.other_background;
                    break;
            }
        }
        if (sportImageResource != 0) {
            imageView.setImageResource(sportImageResource);
        }

        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake_animation);
        cardView.setAnimation(shake);
    }

    void configView(){
        titleTextView = findViewById(R.id.titleTextView);
        location = findViewById(R.id.locationTextView);
        dateTextView = findViewById(R.id.dateTextView);
        availableSpots = findViewById(R.id.availableSpots);
        imageView = findViewById(R.id.imageView);
        cardView = findViewById(R.id.cardView);
    }
}