package com.fitconnect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitconnect.controller.BrightnessController;
import com.fitconnect.controller.BrightnessObserver;
import com.fitconnect.controller.NavigationHandler;
import com.fitconnect.model.user.User;
import com.fitconnect.viewmodel.PhotoViewModel;
import com.fitconnect.viewmodel.SettingsViewModel;
import com.fitconnect.viewmodel.UserViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.Manifest;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileActivity extends AppCompatActivity implements BrightnessObserver{

    String emailFromIntent;
    private TextView userName, points, achivementText;
    private ImageView editPic, achievementPic;

    CircleImageView profilePic;
    private UserViewModel userViewModel;
    private PhotoViewModel photoViewModel;

    private BottomNavigationView bottomNavigationView;

    private BrightnessController brightnessController;
    private SettingsViewModel settingsViewModel;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int PERMISSION_REQUEST_CAMERA = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        configView();

        bottomNavigationView.setSelectedItemId(R.id.navigation_profile);
        NavigationHandler.setupWith(bottomNavigationView, this);

        brightnessController = new BrightnessController(this);

        /*
        Intent intent = getIntent();
        emailFromIntent = intent.getStringExtra("email");
         */

        SharedPreferences sharedPref = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
        emailFromIntent = sharedPref.getString("email", "No hay email");

        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        photoViewModel = new ViewModelProvider(this).get(PhotoViewModel.class);
        settingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        userViewModel.getUserByEmail(emailFromIntent).observe(this, usuario -> {
            if (usuario != null) {
                userName.setText(usuario.getFirstName());
                points.setText(String.valueOf(usuario.getFitconnectPoints()));
                if(usuario.getProfilePicture()!=null &&
                        !usuario.getProfilePicture().equals("")){
                    Picasso.get()
                            .load(usuario.getProfilePicture())
                            .resize(profilePic.getWidth(), profilePic.getHeight())
                            .centerCrop()
                            .into(profilePic);
                }
                if(usuario.getAchievements()!=null && !usuario.getAchievements().isEmpty()){
                    userViewModel.getAchievementById(usuario.getAchievements().get(0)).observe(this,achievement -> {
                            if(achievement.getImage() != null && !achievement.getImage().isEmpty()){
                                Picasso.get()
                                        .load(achievement.getImage())
                                        .resize(achievementPic.getWidth(), achievementPic.getHeight())
                                        .centerCrop()
                                        .into(achievementPic);
                                achievementPic.setVisibility(View.VISIBLE);
                            }
                            if(achievement.getName() != null && !achievement.getName().isEmpty()){
                                achivementText.setText(achievement.getName());
                            }
                    });
                }
            }
        });

        editPic.setOnClickListener(view -> {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNavigationView.setSelectedItemId(R.id.navigation_profile);

        if (settingsViewModel.getBrightnessControlModePref()){
            brightnessController.start();
            brightnessController.addObserver(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (settingsViewModel.getBrightnessControlModePref()){
            brightnessController.stop();
            brightnessController.removeObserver(this);
        }
    }

    @Override
    public void onBrightnessChanged(float brightness) {
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();

        if (brightness < 15) {
            // Baja la luminosidad de la pantalla en un 20%
            layoutParams.screenBrightness = 0.2f;
        } else if (brightness < 530) {
            // Mantiene la luminosidad de la pantalla igual
            layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
        } else {
            // Aumenta la luminosidad de la pantalla en un 20%
            layoutParams.screenBrightness = 1f;
        }
        getWindow().setAttributes(layoutParams);
    }

    void configView(){
        userName = findViewById(R.id.userName);
        points = findViewById(R.id.points);
        profilePic = findViewById(R.id.profilePic);
        editPic = findViewById(R.id.imageView4);
        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        achievementPic =  findViewById(R.id.imageView5);
        achivementText = findViewById(R.id.textView6);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                Toast.makeText(this, "Camera permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            byte[] dataBytes = outputStream.toByteArray();
            Uri photoUri = Uri.fromFile(new File(getCacheDir(), "photo.jpg"));

            try {
                FileOutputStream fileOutputStream = new FileOutputStream(new File(photoUri.getPath()));
                fileOutputStream.write(dataBytes);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            photoViewModel.uploadPhoto(photoUri).observe(this, photoUrl -> {
                if (photoUrl != null) {
                    Log.d("FOTO", "onActivityResult: " + photoUrl);
                    Picasso.get()
                            .load(photoUrl)
                            .resize(profilePic.getWidth(), profilePic.getHeight())
                            .centerCrop()
                            .into(profilePic);

                    // Actualizar la foto de perfil en el ViewModel
                    userViewModel.updateUserProfile(emailFromIntent, photoUrl);
                } else {
                    Toast.makeText(this, "Error uploading photo", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}