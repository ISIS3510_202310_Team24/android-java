package com.fitconnect.controller;

public interface BrightnessObserver {
    void onBrightnessChanged(float brightness);
}
