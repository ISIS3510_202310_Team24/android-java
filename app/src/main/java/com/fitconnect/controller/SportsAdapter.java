package com.fitconnect.controller;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.fitconnect.R;
import com.fitconnect.viewmodel.SportViewModel;

import java.util.List;

public class SportsAdapter extends RecyclerView.Adapter<SportsAdapter.ViewHolder> {

    private List<SportViewModel.SportCardInfo> sports;
    private LayoutInflater inflater;
    public SportsAdapter(Context context, MutableLiveData<List<SportViewModel.SportCardInfo>> sports){
        this.sports = sports.getValue();
        this.inflater = LayoutInflater.from(context);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.sport_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.sport.setText(sports.get(position).getTitle());
        holder.sportImg.setImageResource(sports.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return sports.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView sport;
        public ImageView sportImg;
        public ViewHolder(@NonNull View itemView){
            super(itemView);
            sport = itemView.findViewById(R.id.sport);
            sportImg = itemView.findViewById(R.id.sportImage);
        }
    }
}
