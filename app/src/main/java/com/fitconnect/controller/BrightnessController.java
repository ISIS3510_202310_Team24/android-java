package com.fitconnect.controller;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.WindowManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class BrightnessController implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor lightSensor;
    private List<BrightnessObserver> observers;

    public BrightnessController(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        observers = new ArrayList<>();
    }

    public void start() {
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stop() {
        sensorManager.unregisterListener(this);
    }

    public void addObserver(BrightnessObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(BrightnessObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            float brightness = event.values[0];
            for (BrightnessObserver observer : observers) {
                observer.onBrightnessChanged(brightness);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // No es necesario hacer nada aquí
    }
}


