package com.fitconnect.controller;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import com.fitconnect.ExploreActivity;
import com.fitconnect.HomeActivity;
import com.fitconnect.ProfileActivity;
import com.fitconnect.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class NavigationHandler {

    public static void setupWith(BottomNavigationView bottomNavigationView, AppCompatActivity activity) {
        /*
        if (activity instanceof MainActivity) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_home);
        } else if (activity instanceof DashboardActivity) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_dashboard);
        } else if (activity instanceof NotificationsActivity) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_notifications);
        }*/

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {

                case R.id.navigation_home:
                    if(!(activity instanceof HomeActivity)){
                        Intent intent = new Intent(activity, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        activity.startActivity(intent);
                    }
                    return true;
                case R.id.navigation_profile:
                    if (!(activity instanceof ProfileActivity)) {
                        Intent intent = new Intent(activity, ProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        activity.startActivity(intent);
                    }
                    return true;


                case R.id.navigation_explore:
                    if (!(activity instanceof ExploreActivity)) {
                        Intent intent = new Intent(activity, ExploreActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        activity.startActivity(intent);
                    }
                    return true;

                    /*
                case R.id.navigation_notifications:
                    if (!(activity instanceof NotificationsActivity)) {
                        Intent intent = new Intent(activity, NotificationsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        activity.startActivity(intent);
                    }
                    return true;*/

                default:
                    return false;
            }
        });
    }
}


