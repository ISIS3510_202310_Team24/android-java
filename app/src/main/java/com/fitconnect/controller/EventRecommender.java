package com.fitconnect.controller;

import android.util.Log;

import com.fitconnect.model.user.Event;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventRecommender {
    private List<Event> allEvents; // lista de todos los eventos disponibles
    private List<Event> userEvents; // lista de eventos a los que el usuario se ha inscrito

    public EventRecommender(List<Event> allEvents, List<Event> userEvents) {
        this.allEvents = allEvents;
        this.userEvents = userEvents;
    }

    public Event recommendEvent() {
        Map<Event, Integer> scores = new HashMap<Event, Integer>();

        // Recorre todos los eventos disponibles y calcula una puntuación para cada uno
        for (Event event : allEvents) {
            // Si el usuario no se ha inscrito en el evento, calcula su puntuación
            if (!userEvents.contains(event)) {
                int score = 0;

                // Calcula la puntuación del evento en función de los eventos anteriores del usuario
                for (Event userEvent : userEvents) {
                    // Si el evento es similar al evento anterior, aumenta la puntuación
                    if (event.isSimilarTo(userEvent)) {
                        score++;
                    }
                }

                scores.put(event, score);
            }
        }

        // Encuentra el evento con la puntuación más alta y recomiéndalo
        Event recommendedEvent = null;
        int maxScore = -1;
        for (Map.Entry<Event, Integer> entry : scores.entrySet()) {
            if (entry.getValue() > maxScore) {
                recommendedEvent = entry.getKey();
                maxScore = entry.getValue();
            }
        }
        Log.d("Log-MaxScore", String.valueOf(maxScore));
        return recommendedEvent;
    }
}

