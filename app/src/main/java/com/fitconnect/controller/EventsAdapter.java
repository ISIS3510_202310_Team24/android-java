package com.fitconnect.controller;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.fitconnect.R;
import com.fitconnect.model.user.Event;

import org.w3c.dom.Text;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {
    private List<Event> events;

    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Event event);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public EventsAdapter(List<Event> events) {
        this.events = events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);
        return new ViewHolder(itemView, (EventsAdapter.OnItemClickListener) listener, events);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Event event = events.get(position);
        holder.bind(event);
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private EventsAdapter.OnItemClickListener listener;

        private List<Event> events;
        private TextView titleTextView;
        private TextView dateTextView;

        private TextView availableSpots;
        private TextView location;


        private CardView cardView;

        private ImageView imageView;

        public ViewHolder(@NonNull View itemView, EventsAdapter.OnItemClickListener listener, List<Event> events) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            location = itemView.findViewById(R.id.locationTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            availableSpots = itemView.findViewById(R.id.availableSpots);
            cardView = itemView.findViewById(R.id.cardView);
            imageView = itemView.findViewById(R.id.imageView);
            this.listener = (OnItemClickListener) listener;
            this.events = events;

            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION && this.listener != null) {
                    this.listener.onItemClick(this.events.get(position));
                }
            });
        }

        public void bind(Event event) {
            titleTextView.setText(event.getSport());
            location.setText(event.getLocation());
            dateTextView.setText(event.getStartDate().toString());
            availableSpots.setText(event.getSpotsAvailable().toString());

            int sportImageResource = 0;
            String sport = event.getSport();
            if (sport != null) {
                switch (sport.toLowerCase()) {
                    case "hockey":
                        sportImageResource = R.drawable.hockey_background;
                        break;
                    case "volleyball":
                        sportImageResource = R.drawable.volleyball_background;
                        break;
                    case "boxing":
                        sportImageResource = R.drawable.boxing_background;
                        break;
                    case "baseball":
                        sportImageResource = R.drawable.baseball_background;
                        break;
                    case "basketball":
                        sportImageResource = R.drawable.basketball_background;
                        break;
                    case "tennis":
                        sportImageResource = R.drawable.tennis_background;
                        break;
                    case "football":
                        sportImageResource = R.drawable.football_background;
                        break;
                    // Agrega más casos según los deportes que quieras
                    default:
                        sportImageResource = R.drawable.other_background;
                        break;
                }
            }
            if (sportImageResource != 0) {
                imageView.setImageResource(sportImageResource);
            }
        }
    }
}

