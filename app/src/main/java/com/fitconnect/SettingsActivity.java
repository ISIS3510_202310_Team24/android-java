package com.fitconnect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitconnect.viewmodel.SettingsViewModel;
import com.google.android.material.switchmaterial.SwitchMaterial;

public class SettingsActivity extends AppCompatActivity {
    private SettingsViewModel settingsViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        settingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        Toolbar toolbar = findViewById(R.id.settingsToolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        LinearLayout brightnessControlLayout = findViewById(R.id.brightness_control);
        TextView brightnessControlText = brightnessControlLayout.findViewById(R.id.option_description);
        brightnessControlText.setText("Allow us to manage your screen brightness");
        SwitchMaterial brightnessControlSwitch = brightnessControlLayout.findViewById(R.id.my_switch);

        brightnessControlSwitch.setChecked(settingsViewModel.getBrightnessControlModePref());
        brightnessControlSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
        // Changes preference boolean when switch is switched.
            settingsViewModel.changeBrightnessControlModePref();
        });
        //TBD
        //LinearLayout nightModeLayout = findViewById(R.id.night_mode_pref);
        //TextView nightModeText = nightModeLayout.findViewById(R.id.option_description);
        //nightModeText.setText("Night mode");
        //SwitchMaterial nightModeSwitch = nightModeLayout.findViewById(R.id.my_switch);

        //nightModeSwitch.setChecked(settingsViewModel.getNightModePref());
        //nightModeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            // Changes preference boolean when switch is switched.
            //settingsViewModel.changeNightModePref();
        //});

    }
}