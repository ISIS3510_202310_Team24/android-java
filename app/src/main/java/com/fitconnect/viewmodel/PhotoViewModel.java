package com.fitconnect.viewmodel;

import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.fitconnect.repository.PhotoRepository;
import com.fitconnect.repository.IPhotoRepository;

public class PhotoViewModel extends ViewModel {
    private IPhotoRepository photoRepository;

    public PhotoViewModel() {
        photoRepository = new PhotoRepository();
    }

    public LiveData<String> uploadPhoto(Uri photoUri) {
        return photoRepository.uploadPhoto(photoUri);
    }
}
