package com.fitconnect.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.fitconnect.livedata.UserLiveData;
import com.fitconnect.repository.AuthenticationRepository;
import com.google.firebase.auth.FirebaseUser;

public class AuthViewModel extends AndroidViewModel {

    private AuthenticationRepository authRepository;
    private MutableLiveData<FirebaseUser> userData;
    UserLiveData liveData = null;

    Application application;

    public MutableLiveData<FirebaseUser> getUserData() {
        return userData;
    }

    public AuthViewModel(@NonNull Application application) {
        super(application);
        authRepository = new AuthenticationRepository(application);
        userData = authRepository.getFirebaseUserMutableLiveData();
        this.application=application;
    }

    public void register(String email, String password, String firstName, String lastName){
        authRepository.register(email, password, firstName, lastName);
    }

    public void logIn(String email, String password){
        authRepository.login(email, password);
    }

    public void saveEmailInSharedPreferences(String email){
        SharedPreferences sharedPref = application.getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("email", email);
        editor.apply();
    }

}
