package com.fitconnect.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ViewModelFactory implements ViewModelProvider.Factory {
    private final Application mApplication;

    public ViewModelFactory(Application application) {
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == ExploreViewModel.class) {
            return (T) new ExploreViewModel(mApplication);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}

