package com.fitconnect.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.fitconnect.model.dao.EventDao;
import com.fitconnect.model.user.Event;
import com.fitconnect.repository.EventRepository;
import com.fitconnect.repository.IUserRepository;
import com.fitconnect.repository.UserRepository;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.transform.sax.SAXResult;

public class ExploreViewModel extends ViewModel {
    private EventRepository eventRepository;

    public ExploreViewModel(Application application) {
        eventRepository = new EventRepository(application);
    }

    public LiveData<List<Event>> getEvents() {
        return eventRepository.getEvents();
    }

    public LiveData<List<Event>> getEventsByUser(String userEmail){return eventRepository.getEventsByUser(userEmail);}

    public LiveData<Event> getRecommendedEvent(String userEmail){return eventRepository.getRecommendedEvent(userEmail);}

    public LiveData<String> addParticipantToEvent(String userEmail, String eventId) {
        return eventRepository.addParticipantToEvent(userEmail, eventId);
    }
}

