package com.fitconnect.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fitconnect.model.user.Achievement;
import com.fitconnect.model.user.User;
import com.fitconnect.repository.UserRepository;
import com.fitconnect.repository.IUserRepository;

public class UserViewModel extends ViewModel {
    private IUserRepository userRepository;

    public UserViewModel() {
        userRepository = new UserRepository();
    }

    public LiveData<User> getUserByEmail(String correo) {
        return userRepository.getUserByEmail(correo);
    }

    public MutableLiveData<Boolean> updateUserProfile(String email, String photoUrl) {
        return userRepository.updateUserProfile(email, photoUrl);
    }

    public LiveData<Achievement> getAchievementById(String achievementId){
        return userRepository.getAchievementById(achievementId);
    }
}

