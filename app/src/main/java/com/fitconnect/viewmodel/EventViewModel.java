package com.fitconnect.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.SparseArray;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.fitconnect.repository.EventRepository;
import com.google.android.gms.tasks.Task;

public class EventViewModel extends AndroidViewModel {

    private EventRepository eventRepository;
    private Application application;
    public EventViewModel(@NonNull Application application) {
        super(application);
        eventRepository = new EventRepository(application);
        this.application = application;
    }

    public void saveEventForLaterOffline(int sport, int playersNeeded, int playersBrought, String eventsDate, String eventsTime, String duration, String location){
        SharedPreferences eventFile = application.getSharedPreferences("eventOffline", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = eventFile.edit();
        editor.putInt("sport", sport);
        editor.putInt("playersNeeded", playersNeeded);
        editor.putInt("playersBrought", playersBrought);
        editor.putString("eventsDate", eventsDate);
        editor.putString("eventsTime", eventsTime);
        editor.putString("duration", duration);
        editor.putString("location", location);
        editor.commit();
    }

    public SparseArray<Object> restoreEventSavedOffline(){
        SparseArray<Object> restoredEvent = new SparseArray<Object>(7);
        SharedPreferences eventFile = application.getSharedPreferences("eventOffline", Context.MODE_PRIVATE);
        if(eventFile.getInt("sport", -1) != -1){
            restoredEvent.append(0, eventFile.getInt("sport", 0));
            restoredEvent.append(1, eventFile.getInt("playersNeeded", 0));
            restoredEvent.append(2, eventFile.getInt("playersBrought", 0));
            restoredEvent.append(3, eventFile.getString("eventsDate", ""));
            restoredEvent.append(4, eventFile.getString("eventsTime", ""));
            restoredEvent.append(5, eventFile.getString("duration", ""));
            restoredEvent.append(6, eventFile.getString("location", ""));
            removeEventOffline();
        }
        return restoredEvent;
    }

    private void removeEventOffline(){
        SharedPreferences eventFile = application.getSharedPreferences("eventOffline", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = eventFile.edit();
        editor.remove("sport");
        editor.remove("playersNeeded");
        editor.remove("playersBrought");
        editor.remove("eventsDate");
        editor.remove("eventsTime");
        editor.remove("duration");
        editor.remove("location");
        editor.commit();
    }
    public Task<Void> createEvent(String sport, int playersNeeded, int playersBrought, String eventsDate, String eventsTime, String duration, String location){
        return eventRepository.createEvent(sport, playersNeeded, playersBrought, eventsDate, eventsTime, duration, location);
    }
}
