package com.fitconnect.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.fitconnect.R;

import java.util.ArrayList;
import java.util.List;

public class SportViewModel extends AndroidViewModel {

    private final MutableLiveData<List<SportCardInfo>> sports = new MutableLiveData<>();
    public SportViewModel(@NonNull Application application) {
        super(application);
        setSports();
    }

    public MutableLiveData<List<SportCardInfo>> getSportsData(){
        return sports;
    }

    public void setSports(){
        List<SportCardInfo> sportsList = new ArrayList<>();
        sportsList.add(new SportCardInfo("Baseball", R.drawable.baseball_background ));
        sportsList.add(new SportCardInfo("Basketball", R.drawable.basketball_background ));
        sportsList.add(new SportCardInfo("Football", R.drawable.football_background ));
        sportsList.add(new SportCardInfo("Volleyball", R.drawable.volleyball_background ));
        sportsList.add(new SportCardInfo("Tennis", R.drawable.tennis_background ));
        sportsList.add(new SportCardInfo("Hockey", R.drawable.hockey_background ));
        sportsList.add(new SportCardInfo("Boxing", R.drawable.boxing_background ));
        sports.setValue(sportsList);
    }

    public class SportCardInfo{
        private String title;
        private int image;

        SportCardInfo(String title, int image){
            this.title = title;
            this.image = image;
        }
        public String getTitle(){
            return title;
        }
        public int getImage(){
            return image;
        }
    }


}

