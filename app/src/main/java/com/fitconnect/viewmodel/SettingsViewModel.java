package com.fitconnect.viewmodel;

import static android.content.Context.MODE_PRIVATE;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import java.io.File;

public class SettingsViewModel extends AndroidViewModel {
    private SharedPreferences sP;
    public SettingsViewModel(@NonNull Application application){
        super(application);
        sP = application.getSharedPreferences("settings_preferences", MODE_PRIVATE);
        if (sP.getInt("configured", -1) == -1){
            SharedPreferences.Editor editor = sP.edit();
            // Add all of the preferences options to initialize the file.
            editor.putInt("configured", 1);
            editor.putBoolean("night_mode", false);
            editor.putBoolean("brightness_control", true);
            editor.commit();
        }
    }

    public boolean getNightModePref(){
        return sP.getBoolean("night_mode", false);
    }
    public boolean getBrightnessControlModePref(){
        return sP.getBoolean("brightness_control", false);
    }

    public void changeBrightnessControlModePref(){
        SharedPreferences.Editor editor = sP.edit();
        editor.putBoolean("brightness_control", !sP.getBoolean("brightness_control", false));
        editor.commit();
    }
    public void changeNightModePref(){
        SharedPreferences.Editor editor = sP.edit();
        editor.putBoolean("night_mode", !sP.getBoolean("night_mode", false));
        editor.commit();
    }

}
