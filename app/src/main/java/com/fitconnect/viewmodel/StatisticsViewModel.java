package com.fitconnect.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.fitconnect.repository.EventRepository;

import java.util.HashMap;


public class StatisticsViewModel extends AndroidViewModel {
    private MutableLiveData<Integer> countFailureData;
    private MutableLiveData<HashMap<String, Integer>> sportCount;
    private EventRepository eventRepository;
    Application application;

    public StatisticsViewModel(@NonNull Application application) {
        super(application);
        eventRepository = new EventRepository(application);
        countFailureData = eventRepository.getCountLiveData();
        sportCount = eventRepository.getSportsCount();
        this.application=application;
    }

    public MutableLiveData<Integer> getCountFailureData(){return countFailureData;}
    public void countEventFailures(){eventRepository.getEventFailures();}
    public MutableLiveData<HashMap<String, Integer>> getSportCount(){
        return sportCount;
    }
    public void getSportsCountByWeek(){
        eventRepository.getEventsBySportAndWeek();
    }

}
