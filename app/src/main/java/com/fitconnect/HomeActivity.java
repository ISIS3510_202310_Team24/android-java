package com.fitconnect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitconnect.controller.BrightnessController;
import com.fitconnect.controller.BrightnessObserver;
import com.fitconnect.controller.NavigationHandler;
import com.fitconnect.controller.SportsAdapter;
import com.fitconnect.viewmodel.SettingsViewModel;
import com.fitconnect.viewmodel.SportViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class HomeActivity extends AppCompatActivity implements BrightnessObserver {

    private BottomNavigationView bottomNavigationView;
    private SettingsViewModel settingsViewModel;
    private BrightnessController brightnessController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        SportViewModel sportViewModel = new ViewModelProvider(this).get(SportViewModel.class);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
        RecyclerView sportsList = findViewById(R.id.sports_list);
        NavigationHandler.setupWith(bottomNavigationView, this);
        SportsAdapter sportsAdapter = new SportsAdapter(this, sportViewModel.getSportsData());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false);
        sportsList.setLayoutManager(gridLayoutManager);
        sportsList.setAdapter(sportsAdapter);
        View statsButton = findViewById(R.id.statsButton);
        statsButton.setOnClickListener(v -> {
            Intent i = new Intent(HomeActivity.this, StatsActivity.class);
            startActivity(i);
        });
        View createEventButton = findViewById(R.id.addEventButton);
        ImageView createEventImg = createEventButton.findViewById(R.id.card_image);
        createEventImg.setImageResource(R.drawable.baseline_add_box_24);
        TextView createEventTxt = createEventButton.findViewById(R.id.card_description);
        createEventTxt.setText("Add event");
        createEventButton.setOnClickListener(v -> {
            Intent i = new Intent(HomeActivity.this, CreateEventActivity.class);
            startActivity(i);
        });
        settingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        brightnessController = new BrightnessController(this);
    }

    public void itemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.view_settings){
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
        if (settingsViewModel.getBrightnessControlModePref()){
            brightnessController.start();
            brightnessController.addObserver(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (settingsViewModel.getBrightnessControlModePref()){
            brightnessController.stop();
            brightnessController.removeObserver(this);
        }
    }

    @Override
    public void onBrightnessChanged(float brightness) {
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();

        if (brightness < 15) {
            // Baja la luminosidad de la pantalla en un 20%
            layoutParams.screenBrightness = 0.2f;
        } else if (brightness < 530) {
            // Mantiene la luminosidad de la pantalla igual
            layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
        } else {
            // Aumenta la luminosidad de la pantalla en un 20%
            layoutParams.screenBrightness = 1f;
        }
        getWindow().setAttributes(layoutParams);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", (dialog, id) -> {
                        finishAffinity();
                        System.exit(0);
                    })
                    .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss())
                    .show();
        }

}