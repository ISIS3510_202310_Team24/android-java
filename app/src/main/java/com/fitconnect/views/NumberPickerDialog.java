package com.fitconnect.views;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class NumberPickerDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private OnNumberSetListener mListener;

    public interface OnNumberSetListener {
        void onNumberSet(int hours, int minutes);
    }

    public void setOnNumberSetListener(OnNumberSetListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int initialHours = calendar.get(Calendar.HOUR_OF_DAY);
        int initialMinutes = calendar.get(Calendar.MINUTE);

        NumberPicker hourPicker = new NumberPicker(getActivity());
        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(99);
        hourPicker.setValue(0);

        NumberPicker minutePicker = new NumberPicker(getActivity());
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(59);
        minutePicker.setValue(0);

        TextView hoursLabel = new TextView(getActivity());
        hoursLabel.setText("Hours");
        TextView minutesLabel = new TextView(getActivity());
        minutesLabel.setText("Minutes");
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setGravity(Gravity.CENTER);
        layout.addView(hoursLabel);
        layout.addView(hourPicker);
        layout.addView(minutesLabel);
        layout.addView(minutePicker);

        return new AlertDialog.Builder(getActivity())
                .setTitle("Select your event duration")
                .setView(layout)
                .setPositiveButton("Accept", (dialog, which) -> {
                    int hours = hourPicker.getValue();
                    int minutes = minutePicker.getValue();
                    if (mListener != null) {
                        mListener.onNumberSet(hours, minutes);
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // No se usa en este ejemplo
    }
}
