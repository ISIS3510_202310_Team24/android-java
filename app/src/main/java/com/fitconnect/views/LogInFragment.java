package com.fitconnect.views;

import static android.content.ContentValues.TAG;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fitconnect.ExploreActivity;
import com.fitconnect.HomeActivity;
import com.fitconnect.ProfileActivity;
import com.fitconnect.R;
import com.fitconnect.services.firebase.FirebaseConnections;
import com.fitconnect.viewmodel.AuthViewModel;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

public class LogInFragment extends Fragment {
    private EditText login_email, login_password;
    private Button login_button;
    private TextView signUpRedirectText;
    private NavController navController;
    private AuthViewModel viewModel;
    private boolean isConnected = true;
    private final BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            enableElements(isNetworkConnected());
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        viewModel = new ViewModelProvider((ViewModelStoreOwner) this, (ViewModelProvider.Factory) ViewModelProvider.AndroidViewModelFactory
                .getInstance(getActivity().getApplication())).get(AuthViewModel.class);
        viewModel.getUserData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null){
                        //&& !login_email.getText().toString().isEmpty()
                        //&& !login_password.getText().toString().isEmpty()){
                    //navController.navigate(R.id.action_logInFragment_to_logOutFragment);
                    String email = firebaseUser.getEmail();

                    viewModel.saveEmailInSharedPreferences(email);

                    Intent i = new Intent(getActivity(), HomeActivity.class);
                    startActivity(i);

                }
                else{
                    String email = login_email.getText().toString();

                    viewModel.saveEmailInSharedPreferences(email);

                    Intent i = new Intent(getActivity(), HomeActivity.class);
                    startActivity(i);
                }
            }
        });



    }

    private boolean isNetworkConnected(){
        ConnectivityManager cM = (ConnectivityManager) requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cM.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnected();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_in, container, false);
        login_email = view.findViewById(R.id.login_email);
        login_password = view.findViewById(R.id.login_password);
        signUpRedirectText = view.findViewById(R.id.signUpRedirectText);
        login_button = view.findViewById(R.id.login_button);
        return view;
    }

    private void enableElements(boolean enable){
        login_button.setEnabled(enable);
        if (!enable && isConnected){
            Snackbar.make(getActivity().findViewById(android.R.id.content), "You have no connection, wait until it comes back to login", Snackbar.LENGTH_INDEFINITE).show();
            isConnected = false;
        }else if(enable && !isConnected){
            Snackbar snackConnection = Snackbar.make(getActivity().findViewById(android.R.id.content), "Reconnected to your network", Snackbar.LENGTH_LONG);
            snackConnection.setBackgroundTint(getResources().getColor(R.color.celestial_blue));
            snackConnection.show();
            isConnected = true;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        signUpRedirectText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_logInFragment_to_signUpFragment);
            }
        });

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = login_email.getText().toString();
                String pass = login_password.getText().toString();

                if (!email.isEmpty() && !pass.isEmpty()){
                    viewModel.logIn(email , pass);
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        requireActivity().registerReceiver(networkReceiver, filter);
    }

    @Override
    public void onPause(){
        super.onPause();
        requireActivity().unregisterReceiver(networkReceiver);
    }
}