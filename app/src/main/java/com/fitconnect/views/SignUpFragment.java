package com.fitconnect.views;

import static android.content.ContentValues.TAG;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fitconnect.ProfileActivity;
import com.fitconnect.R;
import com.fitconnect.services.firebase.FirebaseConnections;
import com.fitconnect.viewmodel.AuthViewModel;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseUser;


public class SignUpFragment extends Fragment {

    private EditText signup_firstname, signup_lastName, signup_email, signup_password;
    private Button signup_button;
    private TextView loginRedirectText;
    private NavController navController;
    private AuthViewModel viewModel;
    private boolean isConnected = true;
    private final BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            enableElements(isNetworkConnected());
        }
    };
    private boolean isNetworkConnected(){
        ConnectivityManager cM = (ConnectivityManager) requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cM.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnected();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider((ViewModelStoreOwner) this, (ViewModelProvider.Factory) ViewModelProvider.AndroidViewModelFactory
                .getInstance(getActivity().getApplication())).get(AuthViewModel.class);
        viewModel.getUserData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null
                        && !signup_email.getText().toString().isEmpty()
                        && !signup_password.getText().toString().isEmpty()
                        && !signup_firstname.getText().toString().isEmpty()
                        && !signup_lastName.getText().toString().isEmpty()){

                    String email = firebaseUser.getEmail();
                    Intent i = new Intent(getActivity(), ProfileActivity.class);
                    i.putExtra("email", email);
                    startActivity(i);

                }
                //FirebaseUser changes are only observed in the LoginFragment
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        signup_button = view.findViewById(R.id.signup_button);
        loginRedirectText = view.findViewById(R.id.loginRedirectText);
        signup_email = view.findViewById(R.id.signup_email);
        signup_password = view.findViewById(R.id.signup_password);
        signup_firstname = view.findViewById(R.id.signup_firstname);
        signup_lastName = view.findViewById(R.id.signup_lastName);
        return view;
    }

    private void enableElements(boolean enable){
        signup_button.setEnabled(enable);
        if (!enable && isConnected){
            Snackbar.make(getActivity().findViewById(android.R.id.content), "You have no connection, wait until it comes back to login", Snackbar.LENGTH_INDEFINITE).show();
            isConnected = false;
        }else if(enable && !isConnected){
            Snackbar snackConnection = Snackbar.make(getActivity().findViewById(android.R.id.content), "Reconnected to your network", Snackbar.LENGTH_LONG);
            snackConnection.setBackgroundTint(getResources().getColor(R.color.celestial_blue));
            snackConnection.show();
            isConnected = true;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);

        loginRedirectText.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                navController.navigate(R.id.action_signUpFragment_to_logInFragment);
            }
        });

        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = signup_email.getText().toString();
                String password = signup_password.getText().toString();
                String firstName = signup_firstname.getText().toString();
                String lastName = signup_lastName.getText().toString();

                if(email.isEmpty()){
                    signup_email.setError("Email cannot be empty");
                }
                if(password.isEmpty()){
                    signup_password.setError("Password cannot be empty");
                }
                if(firstName.isEmpty()){
                    signup_firstname.setError("First name cannot be empty");
                }
                if(lastName.isEmpty()){
                    signup_lastName.setError("First name cannot be empty");
                }else{
                    viewModel.register(email, password, firstName, lastName);
                }
            }
        });
    }
    @Override
    public void onResume(){
        super.onResume();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        requireActivity().registerReceiver(networkReceiver, filter);
    }

    @Override
    public void onPause(){
        super.onPause();
        requireActivity().unregisterReceiver(networkReceiver);
    }
}