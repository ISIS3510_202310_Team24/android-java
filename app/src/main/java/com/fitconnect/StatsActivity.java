package com.fitconnect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;

import com.fitconnect.controller.BrightnessController;
import com.fitconnect.controller.BrightnessObserver;
import com.fitconnect.viewmodel.SettingsViewModel;
import com.fitconnect.viewmodel.StatisticsViewModel;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.HashMap;

public class StatsActivity extends AppCompatActivity implements BrightnessObserver {

    private StatisticsViewModel statisticsViewModel;

    private SettingsViewModel settingsViewModel;
    private BrightnessController brightnessController;
    private BarChart barChart;
    private boolean isConnected = true;

    private final BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            fetchDataGraphs(isNetworkConnected());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
        statisticsViewModel = new ViewModelProvider(this).get(StatisticsViewModel.class);
        Toolbar toolbar = findViewById(R.id.statisticsToolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        TextView failureCount = findViewById(R.id.numberOfFails);
        TextView descriptionFailure = findViewById(R.id.descriptionFailure);
        statisticsViewModel.getCountFailureData().observe(this, count ->{
            if (isConnected){
                failureCount.setText(String.format("%d", count));
                descriptionFailure.setVisibility(View.VISIBLE);
            }
        });
        barChart = findViewById(R.id.sports_barchart).findViewById(R.id.most_played_sports_bc);
        barChart.getDescription().setEnabled(false);
        barChart.setDrawValueAboveBar(false);
        Snackbar.make(this.findViewById(android.R.id.content), "Data is being fetch to the graphs", Snackbar.LENGTH_INDEFINITE).show();
        statisticsViewModel.getSportCount().observe(this,sportCount ->{
            BarData data = createCharData(sportCount);
            configureChartAppearance();
            prepareChartData(data);
            Snackbar snackConnection = Snackbar.make(findViewById(android.R.id.content), "Data loaded in graphic", Snackbar.LENGTH_LONG);
            snackConnection.setBackgroundTint(getColor(R.color.celestial_blue));
            snackConnection.show();

        });
        statisticsViewModel.countEventFailures();
        statisticsViewModel.getSportsCountByWeek();
        settingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        brightnessController = new BrightnessController(this);

    }

    private void fetchDataGraphs(boolean enable){
        if (!enable && isConnected){
            Snackbar.make(findViewById(android.R.id.content), "You have no connection, wait until it comes back to build the graphic", Snackbar.LENGTH_SHORT).show();
            isConnected = false;
        }else if(enable && !isConnected){
            Snackbar snackConnection = Snackbar.make(findViewById(android.R.id.content), "Reconnected to your network", Snackbar.LENGTH_LONG);
            snackConnection.setBackgroundTint(getColor(R.color.celestial_blue));
            snackConnection.setAction("RELOAD", view -> {
                recreate();
            });
            snackConnection.show();
            isConnected = true;
        }
    }

    private boolean isNetworkConnected(){
        ConnectivityManager cM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cM.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnected();
    }

    private void prepareChartData(BarData data) {
        barChart.setData(data);

        barChart.invalidate();
    }

    private void configureChartAppearance() {
        barChart.setPinchZoom(false);
        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);

        barChart.getDescription().setEnabled(false);


    }
    public BarData createCharData(HashMap<String, Integer> sportCount){
        int i = 0;
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        for (String key: sportCount.keySet()){
            ArrayList<BarEntry> sports = new ArrayList<>();
            sports.add(new BarEntry(i, sportCount.get(key)));
            BarDataSet set = new BarDataSet(sports, key);
            set.setColor(ColorTemplate.COLORFUL_COLORS[i]);
            dataSets.add(set);

            i++;
        }

        return new BarData(dataSets);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, filter);
        if (settingsViewModel.getBrightnessControlModePref()){
            brightnessController.start();
            brightnessController.addObserver(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkReceiver);
        if (settingsViewModel.getBrightnessControlModePref()){
            brightnessController.stop();
            brightnessController.removeObserver(this);
        }
    }

    @Override
    public void onBrightnessChanged(float brightness) {
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();

        if (brightness < 15) {
            // Baja la luminosidad de la pantalla en un 20%
            layoutParams.screenBrightness = 0.2f;
        } else if (brightness < 530) {
            // Mantiene la luminosidad de la pantalla igual
            layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
        } else {
            // Aumenta la luminosidad de la pantalla en un 20%
            layoutParams.screenBrightness = 1f;
        }
        getWindow().setAttributes(layoutParams);

    }
}