package com.fitconnect.livedata;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.fitconnect.model.user.User;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.Map;
import java.util.List;


public class UserLiveData extends LiveData<User> implements EventListener<DocumentSnapshot> {
    private User userTemp = new User();
    public MutableLiveData<User> user = new MutableLiveData<>();
    private DocumentReference documentReference;

    public UserLiveData(DocumentReference documentReference){
        this.documentReference = documentReference;
    }


    @Override
    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException error) {
        if (documentSnapshot != null && documentSnapshot.exists()){
            Map<String, Object> userMap = documentSnapshot.getData();
            userTemp = new User();
            userTemp.setEmail((String)userMap.get("email"));
            userTemp.setFirstName((String)userMap.get("firstName"));
            userTemp.setLastName((String)userMap.get("lastName"));
            userTemp.setFitconnectPoints((Integer)userMap.get("fitconnectPoints"));
            userTemp.setEventStreak((Integer)userMap.get("eventStreak"));
            userTemp.setProfilePicture((String)userMap.get("profilePicture"));
            userTemp.setAchievements((List<String>) userMap.get("achievements"));
            user.setValue(userTemp);
        }else{
            Log.d("TAG", "error");
        }

    }
}
