package com.fitconnect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fitconnect.databinding.ActivityEventsDistributionBinding;
import com.fitconnect.model.user.Event;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class EventsDistributionActivity extends AppCompatActivity {

    private ActivityEventsDistributionBinding binding;
    List<Event> eventList;

    private SaveImageTask saveImageTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_events_distribution);

        Intent intent = getIntent();
        Event[] eventArray = (Event[]) intent.getSerializableExtra("eventList");
        eventList = Arrays.asList(eventArray);

        // Creating a method setData()
        // to set the text in text view and pie chart
        setData();

        binding.saveButton.setOnClickListener(v -> {
            // Ejecutar el AsyncTask para guardar la imagen
            saveImageTask = new SaveImageTask();
            saveImageTask.execute();
        });
    }

    private void setData(){

        Map<String, Integer> eventosPorTipo = contarEventosPorTipo(eventList);

        binding.tvHockey.setText(Integer.toString(eventosPorTipo.getOrDefault("Hockey", 0)));
        binding.tvVolleyball.setText(Integer.toString(eventosPorTipo.getOrDefault("Volleyball", 0)));
        binding.tvBoxing.setText(Integer.toString(eventosPorTipo.getOrDefault("Boxing", 0)));
        binding.tvBaseball.setText(Integer.toString(eventosPorTipo.getOrDefault("Baseball", 0)));
        binding.tvBasketball.setText(Integer.toString(eventosPorTipo.getOrDefault("Basketball", 0)));
        binding.tvTennis.setText(Integer.toString(eventosPorTipo.getOrDefault("Tennis", 0)));
        binding.tvFootball.setText(Integer.toString(eventosPorTipo.getOrDefault("Football", 0)));
        binding.tvOther.setText(Integer.toString(eventosPorTipo.getOrDefault("Other", 0)));


        // Set the data and color to the pie chart
        binding.piechart.addPieSlice(
                new PieModel(
                        "Hockey",
                        Integer.parseInt(binding.tvHockey.getText().toString()),
                        ContextCompat.getColor(this, R.color.Hockey)));
        binding.piechart.addPieSlice(
                new PieModel(
                        "Volleyball",
                        Integer.parseInt(binding.tvVolleyball.getText().toString()),
                        ContextCompat.getColor(this, R.color.Volleyball)));
        binding.piechart.addPieSlice(
                new PieModel(
                        "Boxing",
                        Integer.parseInt(binding.tvBoxing.getText().toString()),
                        ContextCompat.getColor(this, R.color.Boxing)));
        binding.piechart.addPieSlice(
                new PieModel(
                        "Baseball",
                        Integer.parseInt(binding.tvBaseball.getText().toString()),
                        ContextCompat.getColor(this, R.color.Baseball)));
        binding.piechart.addPieSlice(
                new PieModel(
                        "Basketball",
                        Integer.parseInt(binding.tvBasketball.getText().toString()),
                        ContextCompat.getColor(this, R.color.Basketball)));
        binding.piechart.addPieSlice(
                new PieModel(
                        "Tennis",
                        Integer.parseInt(binding.tvTennis.getText().toString()),
                        ContextCompat.getColor(this, R.color.Tennis)));
        binding.piechart.addPieSlice(
                new PieModel(
                        "Football",
                        Integer.parseInt(binding.tvFootball.getText().toString()),
                        ContextCompat.getColor(this, R.color.Football)));
        binding.piechart.addPieSlice(
                new PieModel(
                        "Other",
                        Integer.parseInt(binding.tvOther.getText().toString()),
                        ContextCompat.getColor(this, R.color.Other)));
        // To animate the pie chart
        binding.piechart.startAnimation();
    }

    private Map<String, Integer> contarEventosPorTipo(List<Event> eventList) {
        Map<String, Integer> eventosPorTipo = new HashMap<>();

        // Recorre la lista de eventos y cuenta los eventos por tipo
        for (Event evento : eventList) {
            String tipoEvento = evento.getSport(); // Obtén el tipo de evento (puedes ajustarlo según la estructura de tu clase Event)

            // Si ya existe el tipo de evento en el mapa, incrementa el contador, de lo contrario, inicializa el contador en 1
            if (eventosPorTipo.containsKey(tipoEvento)) {
                int contador = eventosPorTipo.get(tipoEvento);
                eventosPorTipo.put(tipoEvento, contador + 1);
            } else {
                eventosPorTipo.put(tipoEvento, 1);
            }
        }

        return eventosPorTipo;
    }

    private class SaveImageTask extends AsyncTask<Void, Void, Boolean> {

        private Bitmap bitmap;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Ocultar el botón mientras se guarda la imagen
            binding.saveButton.setVisibility(View.GONE);

            // Capturar la vista actual
            View rootView = getWindow().getDecorView().getRootView();
            rootView.setDrawingCacheEnabled(true);
            bitmap = Bitmap.createBitmap(rootView.getDrawingCache());
            rootView.setDrawingCacheEnabled(false);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            // Guardar la imagen en el almacenamiento externo
            String imageFileName = "event_distribution_image.jpg";
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File imageFile = new File(storageDir, imageFileName);

            try {
                FileOutputStream outputStream = new FileOutputStream(imageFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                outputStream.flush();
                outputStream.close();

                // Escanear la imagen y agregarla a la galería
                MediaScannerConnection.scanFile(
                        EventsDistributionActivity.this,
                        new String[]{imageFile.getAbsolutePath()},
                        new String[]{"image/jpeg"},
                        (path, uri) -> {
                            // Imagen escaneada y agregada correctamente a la galería
                            runOnUiThread(() -> Toast.makeText(EventsDistributionActivity.this, "Image saved successfully", Toast.LENGTH_SHORT).show());
                        });

                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            // Mostrar nuevamente el botón
            binding.saveButton.setVisibility(View.VISIBLE);

            if (success) {
                // Tarea completada exitosamente
                Toast.makeText(EventsDistributionActivity.this, "Image saved successfully", Toast.LENGTH_SHORT).show();
            } else {
                // Error al guardar la imagen
                Toast.makeText(EventsDistributionActivity.this, "Error saving image", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

