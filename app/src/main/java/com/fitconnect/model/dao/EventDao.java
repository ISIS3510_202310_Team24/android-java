package com.fitconnect.model.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import com.fitconnect.model.user.Event;
import java.util.List;

@Dao
public interface EventDao {
    @Query("SELECT * FROM event")
    List<Event> getAll();

    @Insert
    void insertAll(Event... events);

    @Delete
    void delete(Event events);

    @Query("DELETE FROM event")
    void deleteAll();

    @Query("SELECT * FROM event WHERE participants LIKE '%' || :participantId || '%'")
    List<Event> getEventsByParticipant(String participantId);

    @Query("SELECT * FROM event WHERE eventOwner LIKE '%' || :ownerId || '%'")
    List<Event> getEventsByOwnerId(String ownerId);
}
