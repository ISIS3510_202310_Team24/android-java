package com.fitconnect.model.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {
    private String firstName;
    private String lastName;
    private String email;
    private Integer fitconnectPoints = 0;
    private Integer eventStreak = 0;
    private String profilePicture = "";
    //private List<String> achievementsIds = new ArrayList<String>();
    private List<String> achievements = new ArrayList<>();

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    /*
    public List<String> getAchievementsIds() {
        return achievementsIds;
    }

    public void setAchievementsIds(List<String> achievementsIds) {
        this.achievementsIds = achievementsIds;
    }*/

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFitconnectPoints() {
        return fitconnectPoints;
    }

    public void setFitconnectPoints(Integer fitconnectPoints) {
        this.fitconnectPoints = fitconnectPoints;
    }

    public Integer getEventStreak() {
        return eventStreak;
    }

    public void setEventStreak(Integer eventStreak) {
        this.eventStreak = eventStreak;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Map<String, Object> toMap(){
        Map<String, Object> user = new HashMap<String, Object>();
        user.put("firstName", this.getFirstName());
        user.put("lastName", this.getLastName());
        user.put("email", this.getEmail());
        user.put("fitconnectPoints", getFitconnectPoints());
        user.put("eventStreak", getEventStreak());
        user.put("profilePicture", getProfilePicture());
        user.put("achievements", getAchievements());
        return user;
    }
}
