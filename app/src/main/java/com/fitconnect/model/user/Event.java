package com.fitconnect.model.user;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity//(primaryKeys = {"eventOwner", "startDate", "sport"})
public class Event implements Serializable {

    @PrimaryKey
    @NonNull
    String id;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    String eventOwner="";
    String location;
    List<String> participants;
    Integer playersBrought;
    Integer playersNeeded;
    @NonNull
    String sport="";
    Integer spotsAvailable;

    Date endDate;
    @NonNull
    Date startDate = new Date();

    public Event() {
    }

    public String getEventOwner() {
        return eventOwner;
    }

    public void setEventOwner(String eventOwner) {
        this.eventOwner = eventOwner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public void setParticipants(List<String> participants) {
        this.participants = participants;
    }

    public Integer getPlayersBrought() {
        return playersBrought;
    }

    public void setPlayersBrought(Integer playersBrought) {
        this.playersBrought = playersBrought;
    }

    public Integer getPlayersNeeded() {
        return playersNeeded;
    }

    public void setPlayersNeeded(Integer playersNeeded) {
        this.playersNeeded = playersNeeded;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public Integer getSpotsAvailable() {
        return spotsAvailable;
    }

    public void setSpotsAvailable(Integer spotsAvailable) {
        this.spotsAvailable = spotsAvailable;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isSimilarTo(Event otherEvent) {
        if (this.sport.equals(otherEvent.getSport())) {
            return true;
        }
        if(this.location.equals(otherEvent.getLocation())){
            return true;
        }
        else {
            return false;
        }
    }
}
